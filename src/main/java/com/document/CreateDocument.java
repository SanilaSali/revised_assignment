package com.document;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class CreateDocument {

	public static void main(String args[]){
		try {
			  XWPFDocument document = new XWPFDocument(); 
		      FileOutputStream out = new FileOutputStream(new File("D:\\Sanila\\CrackVerbal\\Sample.docx"));
		      XWPFParagraph paragraph = document.createParagraph();
		      XWPFRun run = paragraph.createRun();
		      run.setText("Welcome to my new doc. XWPFDocument created word document. Test success");
		     		      
		      XWPFRun paragraphOneRunThree = paragraph.createRun();
		     // paragraphOneRunThree.setStrike(true);
		      paragraphOneRunThree.setFontSize(30);
		      paragraphOneRunThree.setSubscript(VerticalAlign.SUBSCRIPT);
		      paragraphOneRunThree.setText(" Font Styles");
			  document.write(out);
		      out.close();
		      System.out.println("Document created successfully");
			} catch (Exception e) {
				// TODO: handle exception
			}
	}
}
