<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
   <head>
  	<%@ page isELIgnored="false" %>
      <meta charset="UTF-8">
      <title>Repositoty</title>
      
   </head>
   <body>
  <div>
  <h2><center>Repository Demo</center></h2>
  <div style="float:left;margin-left:20px">
     <form method="POST" action="DocumentServlet">
     	<Label> Enter Document name <i> (ending with .docx)</i> :</Label><input type ="text" name="docName" id="docId"></br>
     	<c:if test="${not empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" rows="24" cols="80">${docContent}</textarea></br>
       </c:if>
       <c:if test="${empty docContent}">
       <textarea style="margin-top:10px" name ="mydoc" rows="24" cols="80"></textarea></br>
       </c:if>
       <button style="margin-top:10px" type="submit"> SAVE </button>
      </form>
   </div>
   <div style="float:right">
        <table width ="400" border="1" cellpadding="5">
            <caption><h3>Version History</h3></caption>
            <tr>
                <th>Name</th>
                <th width = "10%">Version</th>
                <th width = "60%">Created Date</th>
            </tr>
            <c:if test="${not empty versionFiles}">
            <c:forEach var="file" items="${sessionScope['versionFiles']}">
                <tr>
                    <td><c:out value="${file.path}" /></td>
                    <td><a href="DocumentServlet?param1=${file.version}" >${file.version} </a> </td>
                    <td><c:out value="${file.modifiedDate}" /></td>
               </tr>
            </c:forEach>
            </c:if>
          </table>
    </div> 
  </div>
        </body>
</html>